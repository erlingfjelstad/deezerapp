package no.fjelstad.erling.deezerapp.models;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import no.fjelstad.erling.deezerapp.models.ui.TrackViewModel;

@RunWith(JUnit4.class)
public class TrackViewModelTests {
    @Test
    public void trackViewModel_shouldConformToContract() throws Exception {
        EqualsVerifier.forClass(TrackViewModel.class)
                .usingGetClass()
                .suppress(Warning.INHERITED_DIRECTLY_FROM_OBJECT)
                .verify();
    }
}
