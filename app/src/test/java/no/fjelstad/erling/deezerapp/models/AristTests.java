package no.fjelstad.erling.deezerapp.models;

import com.google.gson.Gson;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import no.fjelstad.erling.deezerapp.Inject;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(JUnit4.class)
public class AristTests {

    @Test
    public void artist_shouldConformToContract() throws Exception {
        EqualsVerifier.forClass(Artist.class)
                .usingGetClass()
                .suppress(Warning.INHERITED_DIRECTLY_FROM_OBJECT)
                .verify();

    }

    @Test
    public void artist_shouldMapFromJson() throws Exception {
        Gson gson = Inject.gson();

        Artist artist = gson.fromJson("{\n" +
                "    \"id\": 27,\n" +
                "    \"name\": \"Daft Punk\",\n" +
                "    \"link\": \"http://www.deezer.com/artist/27\",\n" +
                "    \"share\": \"http://www.deezer.com/artist/27?utm_source=deezer&utm_content=artist-27&utm_term=0_1497994899&utm_medium=web\",\n" +
                "    \"picture\": \"http://api.deezer.com/artist/27/image\",\n" +
                "    \"picture_small\": \"http://e-cdn-images.deezer.com/images/artist/f2bc007e9133c946ac3c3907ddc5d2ea/56x56-000000-80-0-0.jpg\",\n" +
                "    \"picture_medium\": \"http://e-cdn-images.deezer.com/images/artist/f2bc007e9133c946ac3c3907ddc5d2ea/250x250-000000-80-0-0.jpg\",\n" +
                "    \"picture_big\": \"http://e-cdn-images.deezer.com/images/artist/f2bc007e9133c946ac3c3907ddc5d2ea/500x500-000000-80-0-0.jpg\",\n" +
                "    \"picture_xl\": \"http://e-cdn-images.deezer.com/images/artist/f2bc007e9133c946ac3c3907ddc5d2ea/1000x1000-000000-80-0-0.jpg\",\n" +
                "    \"nb_album\": 29,\n" +
                "    \"nb_fan\": 3484608,\n" +
                "    \"radio\": true,\n" +
                "    \"tracklist\": \"http://api.deezer.com/artist/27/top?limit=50\",\n" +
                "    \"type\": \"artist\"\n" +
                "}", Artist.class);

        assertThat(artist.id()).isEqualTo(27);
        assertThat(artist.name()).isEqualTo("Daft Punk");
        assertThat(artist.link()).isEqualTo("http://www.deezer.com/artist/27");
        assertThat(artist.picture()).isEqualTo("http://api.deezer.com/artist/27/image");
        assertThat(artist.pictureSmall()).isEqualTo("http://e-cdn-images.deezer.com/images/artist/f2bc007e9133c946ac3c3907ddc5d2ea/56x56-000000-80-0-0.jpg");
        assertThat(artist.pictureMedium()).isEqualTo("http://e-cdn-images.deezer.com/images/artist/f2bc007e9133c946ac3c3907ddc5d2ea/250x250-000000-80-0-0.jpg");
        assertThat(artist.pictureBig()).isEqualTo("http://e-cdn-images.deezer.com/images/artist/f2bc007e9133c946ac3c3907ddc5d2ea/500x500-000000-80-0-0.jpg");
        assertThat(artist.tracklist()).isEqualTo("http://api.deezer.com/artist/27/top?limit=50");
    }
}
