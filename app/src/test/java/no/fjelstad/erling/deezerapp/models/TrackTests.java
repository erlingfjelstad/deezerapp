package no.fjelstad.erling.deezerapp.models;

import com.google.gson.Gson;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import no.fjelstad.erling.deezerapp.Inject;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(JUnit4.class)
public class TrackTests {
    @Test
    public void track_shouldConformToContract() throws Exception {
        EqualsVerifier.forClass(Track.class)
                .usingGetClass()
                .suppress(Warning.INHERITED_DIRECTLY_FROM_OBJECT)
                .verify();

    }

    @Test
    public void track_shouldMapFromJson() throws Exception {
        Gson gson = Inject.gson();

        Track track = gson.fromJson("{\n" +
                        "            \"id\": 13795688,\n" +
                        "            \"readable\": false,\n" +
                        "            \"title\": \"In The Flesh? (2011 Remastered Version)\",\n" +
                        "            \"title_short\": \"In The Flesh?\",\n" +
                        "            \"title_version\": \"(2011 Remastered Version)\",\n" +
                        "            \"isrc\": \"GBN9Y1100095\",\n" +
                        "            \"link\": \"http://www.deezer.com/track/13795688\",\n" +
                        "            \"duration\": 198,\n" +
                        "            \"track_position\": 1,\n" +
                        "            \"disk_number\": 1,\n" +
                        "            \"rank\": 479372,\n" +
                        "            \"explicit_lyrics\": false,\n" +
                        "            \"preview\": \"http://e-cdn-preview-0.deezer.com/stream/02814066447f0fac9e46a41424a1d37b-3.mp3\",\n" +
                        "            \"alternative\": {\n" +
                        "                \"id\": 117581624,\n" +
                        "                \"readable\": true,\n" +
                        "                \"title\": \"In The Flesh? (2011 Remastered Version)\",\n" +
                        "                \"title_short\": \"In The Flesh?\",\n" +
                        "                \"title_version\": \"(2011 Remastered Version)\",\n" +
                        "                \"isrc\": \"GBN9Y1100095\",\n" +
                        "                \"link\": \"http://www.deezer.com/track/117581624\",\n" +
                        "                \"duration\": 198,\n" +
                        "                \"track_position\": 1,\n" +
                        "                \"disk_number\": 1,\n" +
                        "                \"rank\": 516240,\n" +
                        "                \"explicit_lyrics\": false,\n" +
                        "                \"preview\": \"http://e-cdn-preview-0.deezer.com/stream/02814066447f0fac9e46a41424a1d37b-3.mp3\",\n" +
                        "                \"artist\": {\n" +
                        "                    \"id\": 860,\n" +
                        "                    \"name\": \"Pink Floyd\",\n" +
                        "                    \"tracklist\": \"http://api.deezer.com/artist/860/top?limit=50\",\n" +
                        "                    \"type\": \"artist\"\n" +
                        "                },\n" +
                        "                \"type\": \"track\"\n" +
                        "            },\n" +
                        "            \"artist\": {\n" +
                        "                \"id\": 860,\n" +
                        "                \"name\": \"Pink Floyd\",\n" +
                        "                \"tracklist\": \"http://api.deezer.com/artist/860/top?limit=50\",\n" +
                        "                \"type\": \"artist\"\n" +
                        "            },\n" +
                        "            \"type\": \"track\"\n" +
                        "        }"
                , Track.class);


        assertThat(track.diskNumber()).isEqualTo(1);
        assertThat(track.duration()).isEqualTo(198);
        assertThat(track.title()).isEqualTo("In The Flesh? (2011 Remastered Version)");
        assertThat(track.trackPosition()).isEqualTo(1);
        assertThat(track.artist()).isNotNull();
    }
}
