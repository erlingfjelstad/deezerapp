package no.fjelstad.erling.deezerapp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import no.fjelstad.erling.deezerapp.dagger.MyGsonFactory;

public class Inject {
 private Inject() {
  // private constructor
 }

 public static Gson gson() {
  return new GsonBuilder()
          .registerTypeAdapterFactory(MyGsonFactory.create())
          .create();
 }
}