package no.fjelstad.erling.deezerapp.artist;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;
import no.fjelstad.erling.deezerapp.DeezerService;
import no.fjelstad.erling.deezerapp.models.api.ArtistApiModel;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class ArtistPresenterTests {

    @Mock
    private DeezerService deezerService;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private ArtistView artistView;

    @Mock
    Observable<ArtistApiModel> observable;
    // object to test
    private ArtistPresenter artistPresenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        artistPresenter = new ArtistPresenterImpl(deezerService);

        RxAndroidPlugins.setInitMainThreadSchedulerHandler(__ -> Schedulers.trampoline());

    }

    @Test
    public void search_shouldNotifyView() throws Exception {
        when(deezerService.queryArtists(anyString())).thenReturn(observable);

        artistPresenter.attach(artistView);

        artistPresenter.search("abc");

        verify(artistView, times(1)).renderArtists();


    }
}
