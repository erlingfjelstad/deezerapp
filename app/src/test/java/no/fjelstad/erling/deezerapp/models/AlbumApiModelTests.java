package no.fjelstad.erling.deezerapp.models;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import no.fjelstad.erling.deezerapp.models.api.AlbumApiModel;

@RunWith(JUnit4.class)
public class AlbumApiModelTests {
    @Test
    public void albumApiModel_shouldConformToContract() throws Exception {
        EqualsVerifier.forClass(AlbumApiModel.class)
                .usingGetClass()
                .suppress(Warning.INHERITED_DIRECTLY_FROM_OBJECT)
                .verify();

    }
}
