package no.fjelstad.erling.deezerapp.models;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

@RunWith(JUnit4.class)
public class TrackApiModelTests {

    @Test
    public void trackApiModel_shouldConformToContract() throws Exception {
        EqualsVerifier.forClass(Track.class)
                .usingGetClass()
                .suppress(Warning.INHERITED_DIRECTLY_FROM_OBJECT)
                .verify();

    }
}
