package no.fjelstad.erling.deezerapp.models;

import com.google.gson.Gson;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import no.fjelstad.erling.deezerapp.Inject;
import no.fjelstad.erling.deezerapp.models.Album;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(JUnit4.class)
public class AlbumTests {
    @Test
    public void album_shouldConformToContract() throws Exception {
        EqualsVerifier.forClass(Album.class)
                .usingGetClass()
                .suppress(Warning.INHERITED_DIRECTLY_FROM_OBJECT)
                .verify();

    }

    @Test
    public void album_shouldMapFromJson() throws Exception {
        Gson gson = Inject.gson();

        Album album = gson.fromJson("{\n" +
                "            \"id\": 8244118,\n" +
                "            \"title\": \"Human After All (Remixes)\",\n" +
                "            \"link\": \"http://www.deezer.com/album/8244118\",\n" +
                "            \"cover\": \"http://api.deezer.com/album/8244118/image\",\n" +
                "            \"cover_small\": \"http://e-cdn-images.deezer.com/images/cover/f6a4dbf47cb8828c281ed4e63364f99e/56x56-000000-80-0-0.jpg\",\n" +
                "            \"cover_medium\": \"http://e-cdn-images.deezer.com/images/cover/f6a4dbf47cb8828c281ed4e63364f99e/250x250-000000-80-0-0.jpg\",\n" +
                "            \"cover_big\": \"http://e-cdn-images.deezer.com/images/cover/f6a4dbf47cb8828c281ed4e63364f99e/500x500-000000-80-0-0.jpg\",\n" +
                "            \"cover_xl\": \"http://e-cdn-images.deezer.com/images/cover/f6a4dbf47cb8828c281ed4e63364f99e/1000x1000-000000-80-0-0.jpg\",\n" +
                "            \"genre_id\": 106,\n" +
                "            \"fans\": 22571,\n" +
                "            \"release_date\": \"2005-03-01\",\n" +
                "            \"record_type\": \"album\",\n" +
                "            \"tracklist\": \"http://api.deezer.com/album/8244118/tracks\",\n" +
                "            \"explicit_lyrics\": false,\n" +
                "            \"type\": \"album\"\n" +
                "        }", Album.class);

        assertThat(album.id()).isEqualTo(8244118);
        assertThat(album.title()).isEqualTo("Human After All (Remixes)");
        assertThat(album.cover()).isEqualTo("http://api.deezer.com/album/8244118/image");
        assertThat(album.coverSmall()).isEqualTo("http://e-cdn-images.deezer.com/images/cover/f6a4dbf47cb8828c281ed4e63364f99e/56x56-000000-80-0-0.jpg");
        assertThat(album.coverMedium()).isEqualTo("http://e-cdn-images.deezer.com/images/cover/f6a4dbf47cb8828c281ed4e63364f99e/250x250-000000-80-0-0.jpg");
        assertThat(album.coverBig()).isEqualTo("http://e-cdn-images.deezer.com/images/cover/f6a4dbf47cb8828c281ed4e63364f99e/500x500-000000-80-0-0.jpg");
        assertThat(album.coverXl()).isEqualTo("http://e-cdn-images.deezer.com/images/cover/f6a4dbf47cb8828c281ed4e63364f99e/1000x1000-000000-80-0-0.jpg");
        assertThat(album.link()).isEqualTo("http://www.deezer.com/album/8244118");
    }
}
