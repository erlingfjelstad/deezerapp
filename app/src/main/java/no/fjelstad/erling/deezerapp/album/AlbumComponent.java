package no.fjelstad.erling.deezerapp.album;

import dagger.Component;
import no.fjelstad.erling.deezerapp.dagger.AppComponent;
import no.fjelstad.erling.deezerapp.dagger.FragmentScope;

@FragmentScope
@Component(modules = AlbumModule.class, dependencies = AppComponent.class)
public interface AlbumComponent {

    void inject(AlbumFragment albumFragment);
}
