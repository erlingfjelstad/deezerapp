package no.fjelstad.erling.deezerapp.models.api;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import java.util.List;

import no.fjelstad.erling.deezerapp.models.Album;

@AutoValue
public abstract class AlbumApiModel {

    @NonNull
    public abstract List<Album> data();

    @NonNull
    public static TypeAdapter<AlbumApiModel> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_AlbumApiModel.GsonTypeAdapter(gson);
    }

}
