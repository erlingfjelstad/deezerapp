package no.fjelstad.erling.deezerapp.dagger;

import android.content.Context;
import android.support.annotation.NonNull;

import com.squareup.picasso.Picasso;

import dagger.Component;
import no.fjelstad.erling.deezerapp.DeezerService;
import okhttp3.OkHttpClient;

@ApplicationScope
@Component(modules = {
        AppModule.class,
        NetworkModule.class})
public interface AppComponent {

    @NonNull
    Picasso getPicasso();

    @NonNull
    DeezerService getDeezerService();

    @NonNull
    Context context();

    @NonNull
    OkHttpClient okhttpClient();

}
