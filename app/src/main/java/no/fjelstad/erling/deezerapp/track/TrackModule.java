package no.fjelstad.erling.deezerapp.track;

import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import no.fjelstad.erling.deezerapp.DeezerService;
import no.fjelstad.erling.deezerapp.dagger.FragmentScope;

@Module
public class TrackModule {

    @FragmentScope
    @Provides
    TrackPresenter providesTrackPresenter(@NonNull DeezerService deezerService) {
        return new TrackPresenterImpl(deezerService);
    }

}
