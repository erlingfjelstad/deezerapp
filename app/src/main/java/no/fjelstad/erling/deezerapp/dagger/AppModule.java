package no.fjelstad.erling.deezerapp.dagger;

import android.app.Application;
import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    private final Context context;

    public AppModule(Application application) {
        this.context = application;
    }

    @ApplicationScope
    @Provides
    public Context providesAppContext() {
        return context;
    }
}
