package no.fjelstad.erling.deezerapp.models;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

@AutoValue
public abstract class Album implements Parcelable {

    @NonNull
    public abstract Integer id();

    @NonNull
    public abstract String title();

    @NonNull
    public abstract String link();

    @NonNull
    public abstract String cover();

    @Nullable
    @SerializedName("cover_small")
    public abstract String coverSmall();

    @Nullable
    @SerializedName("cover_medium")
    public abstract String coverMedium();

    @Nullable
    @SerializedName("cover_big")
    public abstract String coverBig();

    @Nullable
    @SerializedName("cover_xl")
    public abstract String coverXl();

    @NonNull
    public static TypeAdapter<Album> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_Album.GsonTypeAdapter(gson);
    }
}