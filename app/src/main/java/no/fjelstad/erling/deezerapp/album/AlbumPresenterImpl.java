package no.fjelstad.erling.deezerapp.album;

import android.support.annotation.NonNull;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import no.fjelstad.erling.deezerapp.DeezerService;
import no.fjelstad.erling.deezerapp.common.View;
import no.fjelstad.erling.deezerapp.models.Artist;
import no.fjelstad.erling.deezerapp.models.api.AlbumApiModel;

class AlbumPresenterImpl implements AlbumPresenter {

    private final DeezerService deezerService;
    private AlbumView albumView;
    private CompositeDisposable compositeDisposable;

    AlbumPresenterImpl(@NonNull DeezerService deezerService) {
        this.deezerService = deezerService;
        this.compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void attach(View view) {
        if (view instanceof AlbumView) {
            albumView = (AlbumView) view;
        }
    }

    @Override
    public void detach() {
        albumView = null;
        compositeDisposable.clear();
    }


    @Override
    public void listAlbums(@NonNull Artist artist) {
        compositeDisposable.add(
                deezerService.getAlbumsForArtist(artist.id())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(AlbumApiModel::data)
                        .subscribe(albumView.renderAlbums()));
    }

}
