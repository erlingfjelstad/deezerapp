package no.fjelstad.erling.deezerapp.track;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.functions.Consumer;
import no.fjelstad.erling.deezerapp.App;
import no.fjelstad.erling.deezerapp.R;
import no.fjelstad.erling.deezerapp.common.RecyclerViewHorizontalDivider;
import no.fjelstad.erling.deezerapp.models.Album;
import no.fjelstad.erling.deezerapp.models.Artist;
import no.fjelstad.erling.deezerapp.models.ui.TrackViewModel;

public class TrackFragment extends Fragment implements TrackView, TrackAdapter.OnTrackItemClicked {

    private static final String ARG_ALBUM = "arg:Album";
    private static final String ARG_ARTIST = "arg:Artist";
    private Album album;
    private Artist artist;

    @Inject
    TrackPresenter trackPresenter;

    @BindView(R.id.image_view_album_track)
    AppCompatImageView imageView;

    @BindView(R.id.recycler_view_track)
    RecyclerView recyclerView;

    private Picasso picasso;
    private TrackAdapter trackAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().getParcelable(ARG_ALBUM) != null) {
                album = getArguments().getParcelable(ARG_ALBUM);
            }
            if (getArguments().getParcelable(ARG_ARTIST) != null) {
                artist = getArguments().getParcelable(ARG_ARTIST);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_track, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        setUpRecyclerView(view);

        trackPresenter.queryForTracks(album);
        picasso.load(album.coverXl())
                .fit()
                .into(imageView);

        configureActionBar(album, artist);
    }

    private void configureActionBar(Album album, Artist artist) {
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(album.title());
            actionBar.setSubtitle(artist.name());
        }

    }

    private void setUpRecyclerView(View view) {
        trackAdapter = new TrackAdapter(view.getContext(), this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(trackAdapter);
        recyclerView.addItemDecoration(
                new RecyclerViewHorizontalDivider(recyclerView.getContext())
        );
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // DI
        DaggerTrackComponent.builder()
                .appComponent(App.get(getActivity()).appComponent())
                .build()
                .inject(this);

        picasso = App.get(getActivity()).appComponent().getPicasso();

        trackPresenter.attach(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        trackPresenter.detach();
    }


    public static TrackFragment newInstance(@NonNull Album album, @NonNull Artist artist) {
        TrackFragment trackFragment = new TrackFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_ALBUM, album);
        bundle.putParcelable(ARG_ARTIST, artist);
        trackFragment.setArguments(bundle);

        return trackFragment;
    }

    @Override
    public void onTrackItemClicked(TrackViewModel item) {
        Toast.makeText(this.getContext(), item.title(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public Consumer<List<TrackViewModel>> renderTracks() {
        return trackViewModels -> trackAdapter.swapData(trackViewModels);
    }
}
