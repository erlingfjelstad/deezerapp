package no.fjelstad.erling.deezerapp.artist;

import android.support.annotation.NonNull;

import no.fjelstad.erling.deezerapp.common.Presenter;

public interface ArtistPresenter extends Presenter {
    void search(@NonNull String text);
}
