package no.fjelstad.erling.deezerapp.track;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import no.fjelstad.erling.deezerapp.R;
import no.fjelstad.erling.deezerapp.models.ui.TrackViewModel;

import static no.fjelstad.erling.deezerapp.utils.Utils.convertToReadableTime;

class TrackAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int HEADER = 0;
    private static final int ROW = 1;
    private Context mContext;
    private List<TrackViewModel> trackViewModels;
    private OnTrackItemClicked onTrackItemClicked;

    TrackAdapter(Context context, OnTrackItemClicked onTrackItemClicked) {
        this.mContext = context;
        this.onTrackItemClicked = onTrackItemClicked;
        this.trackViewModels = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        switch (viewType) {
            case HEADER: {
                View view = inflater.inflate(R.layout.recycler_view_track_header, parent, false);
                return new HeaderViewHolder(view);
            }
            case ROW: {
                View view = inflater.inflate(R.layout.recycler_view_track_item, parent, false);
                return new RowViewHolder(view, onTrackItemClicked);
            }
            default:
                throw new IllegalArgumentException("Illegal row value");
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TrackViewModel viewModel = trackViewModels.get(position);
        if (holder instanceof RowViewHolder) {
            ((RowViewHolder) holder).update(viewModel);
        } else if (holder instanceof HeaderViewHolder) {
            ((HeaderViewHolder) holder).update(viewModel);
        }
    }

    @Override
    public int getItemViewType(int position) {
        TrackViewModel viewModel = trackViewModels.get(position);
        if (viewModel.isHeader()) {
            return HEADER;
        } else return ROW;
    }

    @Override
    public int getItemCount() {
        return trackViewModels.size();
    }


    void swapData(@NonNull List<TrackViewModel> tracks) {
        this.trackViewModels.clear();
        this.trackViewModels.addAll(tracks);
        notifyDataSetChanged();
    }

    interface OnTrackItemClicked {
        void onTrackItemClicked(TrackViewModel item);
    }

    static class HeaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.track_item_header)
        AppCompatTextView textView;


        HeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void update(TrackViewModel viewModel) {
            textView.setText(viewModel.title() + " " + viewModel.diskNumber());
        }
    }

    static class RowViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.track_item_view)
        View view;

        @BindView(R.id.text_view_track_number)
        AppCompatTextView trackNumber;

        @BindView(R.id.text_view_track_name)
        AppCompatTextView trackName;

        @BindView(R.id.text_view_track_artist)
        AppCompatTextView trackArtist;

        @BindView(R.id.text_view_track_duration)
        AppCompatTextView trackDuration;

        private final InternalClickListener internalClickListener;


        RowViewHolder(View itemView, OnTrackItemClicked onTrackItemClicked) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            internalClickListener = new InternalClickListener(onTrackItemClicked);
        }

        @SuppressLint("SetTextI18n")
        void update(@NonNull TrackViewModel track) {
            trackNumber.setText(track.trackPosition().toString());
            trackName.setText(track.title());
            trackArtist.setText(track.artistName());
            trackDuration.setText(convertToReadableTime(track.duration()));
            internalClickListener.update(track);
            view.setOnClickListener(internalClickListener);
        }
    }

    private static class InternalClickListener implements View.OnClickListener {

        @NonNull
        private final OnTrackItemClicked onTrackItemClicked;

        @Nullable
        private TrackViewModel track;

        InternalClickListener(@NonNull OnTrackItemClicked onTrackItemClicked) {
            this.onTrackItemClicked = onTrackItemClicked;
        }

        @Override
        public void onClick(View view) {
            if (track != null) {
                onTrackItemClicked.onTrackItemClicked(track);
            }
        }

        void update(@NonNull TrackViewModel track) {
            this.track = track;
        }
    }
}