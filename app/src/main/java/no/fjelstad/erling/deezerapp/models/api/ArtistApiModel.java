package no.fjelstad.erling.deezerapp.models.api;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import java.util.List;

import no.fjelstad.erling.deezerapp.models.Artist;

@AutoValue
public abstract class ArtistApiModel {

    @NonNull
    public abstract List<Artist> data();

    @NonNull
    public static TypeAdapter<ArtistApiModel> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_ArtistApiModel.GsonTypeAdapter(gson);
    }

}
