package no.fjelstad.erling.deezerapp;

import android.app.Activity;
import android.app.Application;

import no.fjelstad.erling.deezerapp.dagger.AppComponent;
import no.fjelstad.erling.deezerapp.dagger.AppModule;
import no.fjelstad.erling.deezerapp.dagger.DaggerAppComponent;

public class App extends Application {
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

    }


    public AppComponent appComponent() {
        return appComponent;
    }

    public void releaseAppComponent() {
        appComponent = null;
    }

    public static App get(Activity activity) {
        return (App) activity.getApplication();
    }
}
