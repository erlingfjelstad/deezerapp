package no.fjelstad.erling.deezerapp.artist;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import no.fjelstad.erling.deezerapp.DeezerService;
import no.fjelstad.erling.deezerapp.common.View;
import no.fjelstad.erling.deezerapp.models.api.ArtistApiModel;

class ArtistPresenterImpl implements ArtistPresenter {

    private ArtistView artistView;

    @Inject
    DeezerService deezerService;

    private final CompositeDisposable compositeDisposable;

    ArtistPresenterImpl(@NonNull DeezerService deezerService) {
        this.deezerService = deezerService;
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void attach(@NonNull View view) {
        if (view instanceof ArtistView) {
            artistView = (ArtistView) view;
        }
    }

    @Override
    public void detach() {
        artistView = null;
        compositeDisposable.clear();
    }

    @Override
    public void search(@NonNull String text) {
        compositeDisposable.add(deezerService.queryArtists(text)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(ArtistApiModel::data)
                .subscribe(artistView.renderArtists(), throwable -> {
                    throw new RuntimeException("Something went wrong with downloading artists");
                }));
    }
}
