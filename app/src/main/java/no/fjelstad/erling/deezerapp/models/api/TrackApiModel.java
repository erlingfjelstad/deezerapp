package no.fjelstad.erling.deezerapp.models.api;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import java.util.List;

import no.fjelstad.erling.deezerapp.models.Track;

@AutoValue
public abstract class TrackApiModel {

    @NonNull
    public abstract List<Track> data();

    @NonNull
    public static TypeAdapter<TrackApiModel> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_TrackApiModel.GsonTypeAdapter(gson);
    }

}
