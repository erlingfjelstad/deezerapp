package no.fjelstad.erling.deezerapp.album;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import no.fjelstad.erling.deezerapp.R;
import no.fjelstad.erling.deezerapp.models.Album;
import no.fjelstad.erling.deezerapp.models.Artist;
import no.fjelstad.erling.deezerapp.track.TrackActivity;

import static no.fjelstad.erling.deezerapp.artist.ArtistActivity.ARG_ARTIST;
import static no.fjelstad.erling.deezerapp.utils.Utils.attachFragment;

public class AlbumActivity extends AppCompatActivity implements AlbumFragment.OnAlbumChosen {

    public static final String ARG_ALBUM = "arg:Album";
    public static final String RETAIN_FRAGMENT = "arg:RetainFragment";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        Artist artist = getIntent().getExtras().getParcelable(ARG_ARTIST);

        attachAlbumFragment(savedInstanceState, artist);
        configureAppBar();

    }

    private void attachAlbumFragment(@Nullable Bundle savedInstanceState, Artist artist) {
        AlbumFragment albumFragment;

        if (savedInstanceState == null && artist != null) {
            albumFragment = AlbumFragment.newInstance(artist);
            attachFragment(getSupportFragmentManager(), albumFragment, RETAIN_FRAGMENT);
        } else {
            albumFragment = (AlbumFragment) getSupportFragmentManager().findFragmentByTag(RETAIN_FRAGMENT);
            attachFragment(getSupportFragmentManager(), albumFragment, RETAIN_FRAGMENT);
        }
    }

    private void configureAppBar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onAlbumChosen(@NonNull Album album, @NonNull Artist artist) {
        Intent intent = new Intent(this, TrackActivity.class);
        intent.putExtra(ARG_ALBUM, album);
        intent.putExtra(ARG_ARTIST, artist);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
