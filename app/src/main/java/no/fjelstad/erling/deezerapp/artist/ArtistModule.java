package no.fjelstad.erling.deezerapp.artist;

import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import no.fjelstad.erling.deezerapp.DeezerService;
import no.fjelstad.erling.deezerapp.dagger.FragmentScope;

@Module
public class ArtistModule {

    @Provides
    @FragmentScope
    public ArtistPresenter providesArtistPresenter(@NonNull DeezerService deezerService) {
        return new ArtistPresenterImpl(deezerService);
    }

}
