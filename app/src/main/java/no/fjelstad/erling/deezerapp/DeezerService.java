package no.fjelstad.erling.deezerapp;

import io.reactivex.Observable;
import no.fjelstad.erling.deezerapp.models.api.AlbumApiModel;
import no.fjelstad.erling.deezerapp.models.api.ArtistApiModel;
import no.fjelstad.erling.deezerapp.models.api.TrackApiModel;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface DeezerService {

    @GET("/search/artist/")
    Observable<ArtistApiModel> queryArtists(@Query("q") String searchQuery);

    @GET("/artist/{id}/albums")
    Observable<AlbumApiModel> getAlbumsForArtist(@Path("id") int id);

    @GET("/album/{id}/tracks")
    Observable<TrackApiModel> getTracksForAlbum(@Path("id") int id);


}
