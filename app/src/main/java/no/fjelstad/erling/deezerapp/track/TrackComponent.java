package no.fjelstad.erling.deezerapp.track;

import dagger.Component;
import no.fjelstad.erling.deezerapp.dagger.AppComponent;
import no.fjelstad.erling.deezerapp.dagger.FragmentScope;

@FragmentScope
@Component(modules = TrackModule.class, dependencies = AppComponent.class)
public interface TrackComponent {
    void inject(TrackFragment trackFragment);
}
