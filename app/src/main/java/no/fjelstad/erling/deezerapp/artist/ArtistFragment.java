package no.fjelstad.erling.deezerapp.artist;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.functions.Consumer;
import no.fjelstad.erling.deezerapp.App;
import no.fjelstad.erling.deezerapp.R;
import no.fjelstad.erling.deezerapp.models.Artist;

public class ArtistFragment extends Fragment implements ArtistView,
        SearchView.OnQueryTextListener, SearchView.OnCloseListener, ArtistAdapter.onArtistClickListener {
    // args for reattaching search query in SearchView
    private static final String ARG_SEARCH_QUERY = "arg:SearchQuery";

    // views
    @BindView(R.id.recycler_view_artist)
    RecyclerView recyclerView;

    Picasso picasso;

    SearchView searchView;
    // recyclerview adapter
    ArtistAdapter artistAdapter;
    // inject presenter
    @Inject
    ArtistPresenter artistPresenter;

    // callback to activity
    private OnArtistChosen callback;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // DI
        DaggerArtistComponent.builder()
                .appComponent(App.get(getActivity()).appComponent())
                .build()
                .inject(this);
        picasso = App.get(getActivity()).appComponent().getPicasso();

        // Attach callback to ArtistActivity
        Activity activity = getActivity();
        try {
            callback = (OnArtistChosen) activity;
        } catch (ClassCastException cce) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnArtistChosen");
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        artistPresenter.detach();
    }

    @Override
    public void onResume() {
        super.onResume();
        artistPresenter.attach(this);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_artist, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(ARG_SEARCH_QUERY)) {
            String currentFilter = savedInstanceState.getString(ARG_SEARCH_QUERY);
            searchView.post(() -> searchView.setQuery(currentFilter, false));
        }

        setUpRecyclerView(view);
    }

    private void setUpRecyclerView(View view) {
        artistAdapter = new ArtistAdapter(view.getContext(), picasso, this);
        recyclerView.setAdapter(artistAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(ARG_SEARCH_QUERY, searchView.getQuery().toString());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        // initialize search view with theme from actionbar
        searchView = new SearchView(((ArtistActivity) getContext())
                .getSupportActionBar().getThemedContext());
        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);
        MenuItemCompat.setActionView(item, searchView);


    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        artistPresenter.search(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        artistPresenter.search(newText);
        return true;
    }

    @Override
    public boolean onClose() {
        return false;
    }


    @Override
    public void onArtistClicked(Artist item) {
        callback.onArtistChosen(item);
    }


    @NonNull
    @Override
    public Consumer<List<Artist>> renderArtists() {
        return artists -> artistAdapter.swapData(artists);
    }

    interface OnArtistChosen {
        void onArtistChosen(@NonNull Artist artist);
    }
}
