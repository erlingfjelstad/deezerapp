package no.fjelstad.erling.deezerapp.artist;

import java.util.List;

import io.reactivex.functions.Consumer;
import no.fjelstad.erling.deezerapp.common.View;
import no.fjelstad.erling.deezerapp.models.Artist;

public interface ArtistView extends View {

    Consumer<List<Artist>> renderArtists();
}
