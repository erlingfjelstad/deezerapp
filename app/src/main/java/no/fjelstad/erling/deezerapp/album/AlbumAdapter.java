package no.fjelstad.erling.deezerapp.album;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import no.fjelstad.erling.deezerapp.R;
import no.fjelstad.erling.deezerapp.models.Album;
import no.fjelstad.erling.deezerapp.models.Artist;

class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.ViewHolder> {

    private Context mContext;
    private Picasso picasso;
    private List<Album> list;
    private OnAlbumClickedListener mListener;

    AlbumAdapter(Context context, Picasso picasso, OnAlbumClickedListener onAlbumClickedListener) {
        this.mContext = context;
        this.picasso = picasso;
        this.mListener = onAlbumClickedListener;
        this.list = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.recycler_view_album_item, parent, false);
        return new ViewHolder(view, picasso, mListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Album album = list.get(position);
        holder.update(album);


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    void swapData(List<Album> data) {
        list.clear();
        list.addAll(data);
        notifyDataSetChanged();
    }

    void filter(String query) {
        List<Album> albumsFiltered = new ArrayList<>();

        int size = list.size();
        for (int i = 0; i < size; i++) {
            Album album = list.get(i);

            if(album.title().contains(query)) {
                albumsFiltered.add(album);
            }
        }

        swapData(albumsFiltered);
    }

    interface OnAlbumClickedListener {
        void onAlbumClicked(Album item);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.view_item_album)
        View view;

        @BindView(R.id.image_view_item_album)
        AppCompatImageView imageView;

        @BindView(R.id.text_view_item_album)
        AppCompatTextView textView;

        private final Picasso picasso;
        private final InternalClickListener internalClickListener;

        ViewHolder(View itemView, Picasso picasso, OnAlbumClickedListener onAlbumClickedListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.picasso = picasso;
            this.internalClickListener = new InternalClickListener(onAlbumClickedListener);
        }

        void update(@NonNull Album album) {
            textView.setText(album.title());
            picasso.load(album.coverBig()).into(imageView);
            internalClickListener.update(album);
            view.setOnClickListener(internalClickListener);

        }
    }

    private static class InternalClickListener implements View.OnClickListener {

        @NonNull
        private final OnAlbumClickedListener onAlbumClickedListener;

        @Nullable
        private Album album;

        InternalClickListener(@NonNull OnAlbumClickedListener onAlbumClickedListener) {
            this.onAlbumClickedListener = onAlbumClickedListener;
        }

        @Override
        public void onClick(View view) {
            if (album != null) {
                onAlbumClickedListener.onAlbumClicked(album);
            }
        }

        void update(@NonNull Album album) {
            this.album = album;
        }
    }
}