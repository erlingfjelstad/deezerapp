package no.fjelstad.erling.deezerapp.album;

import android.support.annotation.NonNull;

import no.fjelstad.erling.deezerapp.common.Presenter;
import no.fjelstad.erling.deezerapp.models.Artist;

public interface AlbumPresenter extends Presenter {
    void listAlbums(@NonNull Artist artist);
}
