package no.fjelstad.erling.deezerapp.utils;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import java.util.Locale;

import no.fjelstad.erling.deezerapp.R;

public class Utils {

    public static void attachFragment(@NonNull FragmentManager fragmentManager,
                                      @NonNull Fragment fragment,
                                      @NonNull String tag) {
        fragmentManager
                .beginTransaction()
                .replace(R.id.content_frame, fragment, tag)
                .commit();
    }

    public static String convertToReadableTime(int duration) {
        int minutes = (duration % 3600) / 60;
        int seconds = duration % 60;

        return String.format(Locale.US, "%02d:%02d", minutes, seconds);
    }
}
