package no.fjelstad.erling.deezerapp.models;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

@AutoValue
public abstract class Artist implements Parcelable {

    @NonNull
    public abstract Integer id();

    @NonNull
    public abstract String name();

    @Nullable
    public abstract String link();

    @Nullable
    public abstract String picture();

    @Nullable
    @SerializedName("picture_small")
    public abstract String pictureSmall();

    @Nullable
    @SerializedName("picture_medium")
    public abstract String pictureMedium();

    @Nullable
    @SerializedName("picture_big")
    public abstract String pictureBig();

    @Nullable
    public abstract String type();

    @NonNull
    public abstract String tracklist();

    @NonNull
    public static TypeAdapter<Artist> typeAdapter(Gson gson) {
        return new AutoValue_Artist.GsonTypeAdapter(gson);
    }

}
