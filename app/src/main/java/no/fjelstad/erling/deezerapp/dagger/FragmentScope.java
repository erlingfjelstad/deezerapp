package no.fjelstad.erling.deezerapp.dagger;

import javax.inject.Scope;

@Scope
public @interface FragmentScope {
}
