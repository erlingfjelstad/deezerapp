package no.fjelstad.erling.deezerapp.track;

import java.util.List;

import io.reactivex.functions.Consumer;
import no.fjelstad.erling.deezerapp.common.View;
import no.fjelstad.erling.deezerapp.models.ui.TrackViewModel;

interface TrackView extends View {
    Consumer<List<TrackViewModel>> renderTracks();
}
