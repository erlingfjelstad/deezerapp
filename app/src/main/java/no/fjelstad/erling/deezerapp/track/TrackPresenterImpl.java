package no.fjelstad.erling.deezerapp.track;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import no.fjelstad.erling.deezerapp.DeezerService;
import no.fjelstad.erling.deezerapp.common.View;
import no.fjelstad.erling.deezerapp.models.Album;
import no.fjelstad.erling.deezerapp.models.Track;
import no.fjelstad.erling.deezerapp.models.api.TrackApiModel;
import no.fjelstad.erling.deezerapp.models.ui.TrackViewModel;

class TrackPresenterImpl implements TrackPresenter {

    private final DeezerService deezerService;

    private TrackView trackView;

    private CompositeDisposable compositeDisposable;

    TrackPresenterImpl(@NonNull DeezerService deezerService) {
        this.deezerService = deezerService;
        this.compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void attach(View view) {
        if (view instanceof TrackView) {
            trackView = (TrackView) view;
        }
    }

    @Override
    public void detach() {
        trackView = null;
        compositeDisposable.clear();
    }

    @Override
    public void queryForTracks(@NonNull Album album) {
        compositeDisposable.add(deezerService.getTracksForAlbum(album.id())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(TrackApiModel::data)
                .map(this::convertToViewModels)
                .subscribe(trackView.renderTracks(), throwable -> {
                    throw new RuntimeException("Something went wrong with downloading tracks");
                }));
    }


    private List<TrackViewModel> convertToViewModels(List<Track> tracks) {
        List<TrackViewModel> viewModels = new ArrayList<>(tracks.size());

        int size = tracks.size();
        for (int i = 0; i < size; i++) {
            Track track = tracks.get(i);

            if (track.trackPosition() == 1 && track.diskNumber() > 0) {
                viewModels.add(TrackViewModel.create(0, "Volume", "", track.diskNumber(), 0, true));
            }
            viewModels.add(TrackViewModel.create(
                    track.trackPosition(),
                    track.title(),
                    track.artist().name(),
                    track.diskNumber(),
                    track.duration(),
                    false));

        }
        return viewModels;
    }
}
