package no.fjelstad.erling.deezerapp.models;

import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

@AutoValue
public abstract class Track implements Parcelable {

    @NonNull
    public abstract String title();

    @NonNull
    public abstract Artist artist();

    @NonNull
    @SerializedName("track_position")
    public abstract Integer trackPosition();

    @NonNull
    public abstract Integer duration();

    @NonNull
    @SerializedName("disk_number")
    public abstract Integer diskNumber();

    @NonNull
    public static TypeAdapter<Track> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_Track.GsonTypeAdapter(gson);
    }
}
