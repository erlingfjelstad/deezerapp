package no.fjelstad.erling.deezerapp.artist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import no.fjelstad.erling.deezerapp.R;
import no.fjelstad.erling.deezerapp.album.AlbumActivity;
import no.fjelstad.erling.deezerapp.models.Artist;

import static no.fjelstad.erling.deezerapp.utils.Utils.attachFragment;

public class ArtistActivity extends AppCompatActivity
        implements ArtistFragment.OnArtistChosen {

    public static final String ARG_ARTIST = "arg:Artist";
    private static final String RETAIN_FRAGMENT = "arg:ArtistFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist);

        ArtistFragment artistFragment;

        if (savedInstanceState == null) {
            artistFragment = new ArtistFragment();
            attachFragment(getSupportFragmentManager(), artistFragment, RETAIN_FRAGMENT);
        } else {
            artistFragment = (ArtistFragment) getSupportFragmentManager().findFragmentByTag(RETAIN_FRAGMENT);
            attachFragment(getSupportFragmentManager(), artistFragment, RETAIN_FRAGMENT);
        }

    }


    @Override
    public void onArtistChosen(@NonNull Artist artist) {
        Intent intent = new Intent(this, AlbumActivity.class);
        intent.putExtra(ARG_ARTIST, artist);
        startActivity(intent);
    }

}
