package no.fjelstad.erling.deezerapp.artist;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import no.fjelstad.erling.deezerapp.R;
import no.fjelstad.erling.deezerapp.models.Artist;

class ArtistAdapter extends RecyclerView.Adapter<ArtistAdapter.ViewHolder> {

    private Context context;
    private List<Artist> list;
    private onArtistClickListener onArtistClickListener;
    private final Picasso picasso;

    @Inject
    ArtistAdapter(Context context, Picasso picasso, onArtistClickListener onArtistClickListener) {
        this.context = context;
        this.picasso = picasso;
        this.list = new ArrayList<>();
        this.onArtistClickListener = onArtistClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.recycler_view_artist_item, parent, false);
        return new ViewHolder(view, onArtistClickListener, picasso);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.update(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    void swapData(List<Artist> artists) {
        list.clear();
        list.addAll(artists);

        notifyDataSetChanged();
    }

    interface onArtistClickListener {
        void onArtistClicked(Artist item);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.view_item_artist)
        View view;

        @BindView(R.id.text_view_item_artist)
        AppCompatTextView label;

        @BindView(R.id.image_view_item_artist)
        AppCompatImageView imageView;

        private final InternalClickListener internalClickListener;

        private final Picasso picasso;

        //TODO Bind views
        ViewHolder(View itemView, onArtistClickListener outerClickListener, Picasso picasso) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.internalClickListener = new InternalClickListener(outerClickListener);
            this.picasso = picasso;
        }

        void update(Artist artist) {
            label.setText(artist.name());
            picasso.load(artist.pictureMedium()).into(imageView);
            internalClickListener.update(artist);
            view.setOnClickListener(internalClickListener);

        }
    }

    private static class InternalClickListener implements View.OnClickListener {

        @NonNull
        private final onArtistClickListener onArtistClickListener;

        @Nullable
        private Artist artist;

        InternalClickListener(@NonNull onArtistClickListener onArtistClickListener) {
            this.onArtistClickListener = onArtistClickListener;
        }

        @Override
        public void onClick(View view) {
            if (artist != null) {
                onArtistClickListener.onArtistClicked(artist);
            }
        }

        void update(@NonNull Artist artist) {
            this.artist = artist;
        }
    }
}