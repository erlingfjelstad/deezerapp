package no.fjelstad.erling.deezerapp.track;

import android.support.annotation.NonNull;

import no.fjelstad.erling.deezerapp.common.Presenter;
import no.fjelstad.erling.deezerapp.models.Album;


interface TrackPresenter extends Presenter {
    void queryForTracks(@NonNull Album album);
}
