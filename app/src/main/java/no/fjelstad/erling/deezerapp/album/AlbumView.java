package no.fjelstad.erling.deezerapp.album;

import java.util.List;

import io.reactivex.functions.Consumer;
import no.fjelstad.erling.deezerapp.common.View;
import no.fjelstad.erling.deezerapp.models.Album;
import no.fjelstad.erling.deezerapp.models.Artist;

public interface AlbumView extends View {
    Consumer<List<Album>> renderAlbums();
}
