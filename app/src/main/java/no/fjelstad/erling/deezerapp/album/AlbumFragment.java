package no.fjelstad.erling.deezerapp.album;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.functions.Consumer;
import no.fjelstad.erling.deezerapp.App;
import no.fjelstad.erling.deezerapp.R;
import no.fjelstad.erling.deezerapp.models.Album;
import no.fjelstad.erling.deezerapp.models.Artist;

public class AlbumFragment extends Fragment implements AlbumView, AlbumAdapter.OnAlbumClickedListener {

    private static final String ARG_ARTIST = "extra:Artist";

    Picasso picasso;

    @Inject
    AlbumPresenter albumPresenter;

    @BindView(R.id.recycler_view_album)
    RecyclerView recyclerView;


    private AlbumAdapter albumAdapter;

    private OnAlbumChosen callback;

    public static AlbumFragment newInstance(@NonNull Artist artist) {
        AlbumFragment albumFragment = new AlbumFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_ARTIST, artist);
        albumFragment.setArguments(bundle);
        return albumFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // DI
        DaggerAlbumComponent.builder()
                .appComponent(App.get(getActivity()).appComponent())
                .build()
                .inject(this);

        picasso = App.get(getActivity()).appComponent().getPicasso();
        albumPresenter.attach(this);

        // Attach callback to ArtistActivity
        Activity activity = getActivity();
        try {
            callback = (OnAlbumChosen) activity;
        } catch (ClassCastException cce) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnAlbumChosen");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        albumPresenter.detach();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_album, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);


        configureActionBar();
        setUpRecyclerView(view);

        albumPresenter.listAlbums(artist());
    }

    private Artist artist() {
        Artist artist = null;
        if (getArguments() != null && getArguments().getParcelable(ARG_ARTIST) != null) {
            artist = getArguments().getParcelable(ARG_ARTIST);
        }
        return artist;
    }

    private void configureActionBar() {
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(artist().name());
            actionBar.setSubtitle(getContext().getString(R.string.albums));
        }
    }


    private void setUpRecyclerView(View view) {
        albumAdapter = new AlbumAdapter(view.getContext(), picasso, this);
        recyclerView.setAdapter(albumAdapter);
        int numberOfColumns = 2;
        recyclerView.setLayoutManager(new GridLayoutManager(recyclerView.getContext(), numberOfColumns));
    }

    @Override
    public void onAlbumClicked(Album album) {
        //TODO: Navigate to track screen
        callback.onAlbumChosen(album, artist());
    }

    @Override
    public Consumer<List<Album>> renderAlbums() {
        return data -> albumAdapter.swapData(data);
    }

    interface OnAlbumChosen {
        void onAlbumChosen(@NonNull Album album, @NonNull Artist artist);
    }
}
