package no.fjelstad.erling.deezerapp.common;

public interface Presenter {
    void attach(View view);

    void detach();
}
