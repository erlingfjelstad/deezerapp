package no.fjelstad.erling.deezerapp.artist;

import dagger.Component;
import no.fjelstad.erling.deezerapp.dagger.AppComponent;
import no.fjelstad.erling.deezerapp.dagger.FragmentScope;

@FragmentScope
@Component(modules = ArtistModule.class, dependencies = AppComponent.class)
public interface ArtistComponent {

    void inject(ArtistFragment artistFragment);

}
