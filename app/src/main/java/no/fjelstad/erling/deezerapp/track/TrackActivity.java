package no.fjelstad.erling.deezerapp.track;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import no.fjelstad.erling.deezerapp.R;
import no.fjelstad.erling.deezerapp.models.Album;
import no.fjelstad.erling.deezerapp.models.Artist;

import static no.fjelstad.erling.deezerapp.album.AlbumActivity.ARG_ALBUM;
import static no.fjelstad.erling.deezerapp.artist.ArtistActivity.ARG_ARTIST;
import static no.fjelstad.erling.deezerapp.utils.Utils.attachFragment;

public class TrackActivity extends AppCompatActivity {
    public static final String RETAIN_FRAGMENT = "arg:RetainFragment";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track);
        Album album = getIntent().getExtras().getParcelable(ARG_ALBUM);
        Artist artist = getIntent().getExtras().getParcelable(ARG_ARTIST);


        attachTrackFragment(savedInstanceState, album, artist);
        configureAppBar();
    }

    private void attachTrackFragment(@Nullable Bundle savedInstanceState, Album album, Artist artist) {
        TrackFragment trackFragment;

        if (savedInstanceState == null && artist != null && album != null) {
            trackFragment = TrackFragment.newInstance(album, artist);
            attachFragment(getSupportFragmentManager(), trackFragment, RETAIN_FRAGMENT);
        } else {
            trackFragment = (TrackFragment) getSupportFragmentManager().findFragmentByTag(RETAIN_FRAGMENT);
            attachFragment(getSupportFragmentManager(), trackFragment, RETAIN_FRAGMENT);
        }
    }

    private void configureAppBar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
