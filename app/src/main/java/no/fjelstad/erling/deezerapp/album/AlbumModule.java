package no.fjelstad.erling.deezerapp.album;

import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import no.fjelstad.erling.deezerapp.DeezerService;
import no.fjelstad.erling.deezerapp.dagger.FragmentScope;

@Module
public class AlbumModule {

    @Provides
    @FragmentScope
    public AlbumPresenter providesAlbumPresenter(@NonNull DeezerService deezerService) {
        return new AlbumPresenterImpl(deezerService);
    }
}
