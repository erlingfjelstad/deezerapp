package no.fjelstad.erling.deezerapp.models.ui;

import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class TrackViewModel implements Parcelable {

    @NonNull
    public abstract Integer trackPosition();

    @NonNull
    public abstract String title();

    @NonNull
    public abstract String artistName();

    @NonNull
    public abstract Integer diskNumber();

    @NonNull
    public abstract Integer duration();

    @NonNull
    public abstract Boolean isHeader();

    public static TrackViewModel create(@NonNull Integer trackPosition,
                                        @NonNull String title,
                                        @NonNull String artistName,
                                        @NonNull Integer diskNumber,
                                        @NonNull Integer duration,
                                        @NonNull Boolean isHeader) {
        return new AutoValue_TrackViewModel(trackPosition, title, artistName, diskNumber, duration, isHeader);
    }


}
