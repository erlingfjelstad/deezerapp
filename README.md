Known bugs: 
ArtistFragment: Mangler sticky RecyclerView header "(ikon) Artist"
Liten UI bug: TrackFragment: når man roterer skjermen til landscape.
State bug: ArtistFragment: Mister scroll posisjon når man roterer skjermen når man søker

Hvis jeg hadde hatt mer tid: 
1) Fullføre tester. Akkurat nå har jeg bare tester for modeller + 1 presenter (for å vise at jeg kan..)
2) Espresso tests for UI
3) TrackFragment: Legge ImageView inn i AppBarLayout
4) Offline support. Lagre objektene i database for offline bruk. Kan her innføre et nytt repository-lag i appen som bestemmer om den skal hente fra lokal database eller fra API
5) Kotlin (ihvertfall for data modeller)
6) Sette opp CI pipeline